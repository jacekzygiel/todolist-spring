package com.zygiel.todolist.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="todoitem")
public class TodoItem {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String description;
    private boolean done;

    public TodoItem(String title, String description, boolean done) {
        this.title = title;
        this.description = description;
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoItem todoItem = (TodoItem) o;
        return done == todoItem.done &&
                Objects.equals(id, todoItem.id) &&
                Objects.equals(title, todoItem.title) &&
                Objects.equals(description, todoItem.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, done, description);
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", status=" + done +
                ", description='" + description + '\'' +
                '}';
    }
}
