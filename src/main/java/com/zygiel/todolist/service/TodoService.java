package com.zygiel.todolist.service;

import com.sun.tools.javac.comp.Todo;
import com.zygiel.todolist.entity.TodoItem;
import com.zygiel.todolist.repository.TodoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TodoService {

    TodoRepository repository;

    public List<TodoItem> retrieveAllTodoItems() {
        return repository.findAll();
    }

    public TodoItem saveItem(TodoItem todoItem) {
        return repository.save(todoItem);
    }

    public Optional<TodoItem> retrieveById(Long id) {
        return repository.findById(id);
    }

    public TodoItem updateItem(Long id, TodoItem receivedTodoItem) {
        Optional<TodoItem> item = repository.findById(id);
        if(item.isPresent()) {
            TodoItem currentItem = item.get();
            currentItem.setTitle(receivedTodoItem.getTitle());
            currentItem.setDescription(receivedTodoItem.getDescription());
            currentItem.setDone(receivedTodoItem.isDone());
            return repository.save(currentItem);
        }
       return null;
    }
}
