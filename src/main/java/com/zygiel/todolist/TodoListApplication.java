package com.zygiel.todolist;

import com.zygiel.todolist.entity.TodoItem;
import com.zygiel.todolist.repository.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TodoListApplication {

	private static final Logger log = LoggerFactory.getLogger(TodoListApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TodoListApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(TodoRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new TodoItem("Buy milk",  "3.2% milk bottle", false));
			repository.save(new TodoItem("Buy chocolate", "Milka milk chocolate", false));


			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (TodoItem item : repository.findAll()) {
				log.info(item.toString());
			}
			log.info("");

//			// fetch an individual customer by ID
//			Customer customer = repository.findById(1L);
//			log.info("Customer found with findById(1L):");
//			log.info("--------------------------------");
//			log.info(customer.toString());
//			log.info("");

//			// fetch customers by last name
//			log.info("Customer found with findByLastName('Bauer'):");
//			log.info("--------------------------------------------");
//			repository.findByLastName("Bauer").forEach(bauer -> {
//				log.info(bauer.toString());
//			});
//			// for (Customer bauer : repository.findByLastName("Bauer")) {
//			//  log.info(bauer.toString());
//			// }
			log.info("");
		};
	}


}
