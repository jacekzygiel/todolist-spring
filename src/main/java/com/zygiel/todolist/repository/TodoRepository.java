package com.zygiel.todolist.repository;

import com.zygiel.todolist.entity.TodoItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TodoRepository extends CrudRepository<TodoItem, Long> {
    List<TodoItem> findAll();
    Optional<TodoItem> findById(Long id);
    TodoItem save(TodoItem todoItem);
}
