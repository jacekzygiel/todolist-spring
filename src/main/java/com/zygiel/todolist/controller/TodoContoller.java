package com.zygiel.todolist.controller;

import com.zygiel.todolist.entity.TodoItem;
import com.zygiel.todolist.service.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/todo/api/v1.0")
public class TodoContoller {

    private TodoService todoService;

    public TodoContoller(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/getAll")
    private List<TodoItem> getAllTodoItems() {
        return todoService.retrieveAllTodoItems();
    }

    @GetMapping("/getById/{id}")
    private Optional<TodoItem> getTodoById(@PathVariable Long id) {
        return todoService.retrieveById(id);
    }

    @PostMapping("/newItem")
    private TodoItem saveToDoItem(@RequestBody TodoItem todoItem) {
        return todoService.saveItem(todoItem);
    }

    @PutMapping("/update/{id}")
    private TodoItem updateToDoItem(@PathVariable Long id, @RequestBody TodoItem todoItem) {
        return todoService.updateItem(id, todoItem);
    }
}
